package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.teamdev.jxmaps.LatLng;

import GeneradorMapa.Mapa;
import model.data_structures.Arco;
import model.data_structures.ArregloDinamico;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.IteratorQueue;
import model.data_structures.Queue;
import model.data_structures.Vertice;
import model.vo.CordVO;
import model.vo.VOMovingViolations;
import model.vo.pesoWay;
import sax.CargaXml;
import view.MovingViolationsManagerView;
/**
 * Clase que representa el controlador 
 * @author nicot
 */
public class Controller 
{
	/**
	 *  Interfaz visual del programa .
	 */
	private MovingViolationsManagerView view;
	/**
	 * Elemento del api sax2 de que carga el api grafoNoDirigido
	 */
	private CargaXml cargaXml;
	/**
	 * Grafo a cargar con los datos de las vias.
	 */
	private GrafoNoDirigido<String, CordVO, pesoWay> grafoVias;
	/**
	 * Constructor del controlador
	 */
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		grafoVias=new GrafoNoDirigido<String, CordVO, pesoWay>();
		cargaXml=new CargaXml(grafoVias);
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				view.printMessage("Elegir un archivo xml para cargar:");
				view.printMessage("1.Central-WashingtonDC-OpenStreetMap.xml");
				view.printMessage("2.map.xml");
				int eleccion = sc.nextInt();
				loadXml(eleccion);
				break;
			case 2:
				if(grafoVias.V()!=0)
				{
					view.printMessage("Generando formato Json...");
					saveJson();
					view.printMessage("Json generado (Revisar carpeta data)");
				}
				else
				{
					view.printMessage("No existe el grafo a guardar");
				}
				break;
			case 3:
				view.printMessage("Cargando formato Json...");
				loadJson();
				view.printMessage("Grafo generado");
				break;

			case 4:	
				if(grafoVias.V()!=0)
				{
					//Coordenadas pasadas por requerimento 4 del taller
					LatLng min = new LatLng(38.8847,-77.0542);
					LatLng max = new LatLng(38.9135,-76.9976);
					generarMapaGrafo(grafoVias, min, max);
					System.out.print("Por favor,espere unos minutos mientras se genera el mapa ");
					try 
					{
						for(int i=0;i<10;i++)
						{
							TimeUnit.SECONDS.sleep(1);
							System.out.print(".");
						}
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				else
				{
					view.printMessage("No existe el grafo, por lo tanto no se puede graficar.");
				}
				break;
			case 5:	
				fin = true;
				sc.close();
				break;
			}
		}
	}
	/**
	 * 	Carga los csv del cuatrimestre. 
	 */
	public void loadXml(int pEleccion) 
	{	
		String[] rta;
		String[] args1= {"data/Central-WashingtonDC-OpenStreetMap.xml"};
		String[] args2= {"data/map.xml"};

		rta=pEleccion==1?args1:pEleccion==2?args2:null;
		if(rta!=null)
		{
			cargaXml.loadXml(rta);
			System.out.println("El grafo se cargo con los siguientes valores: "+grafoVias.toString()); //Imprime el numero de vertices y el tama�o del grafo.
		}
		else
			view.printMessage("La opcion de entrada no es valida");
	}
	/**
	 * Genera un formato Json para guardar el grafo.
	 */
	public void saveJson() 
	{
		JsonObject obj = new JsonObject();

		JsonObject vertices = new JsonObject();
		int numVer = grafoVias.V();
		for(int i = 0; i < numVer; i++) 
		{
			JsonObject ver = new JsonObject();
			String key = grafoVias.keys().darElemento(i);
			Vertice<String,CordVO,pesoWay> vert = grafoVias.darVertice(key);

			ver.addProperty("ID",key);
			ver.addProperty("ADJ", vert.darAdyacentes().darTamano());
			ver.addProperty("LAT", vert.getInfo().getLat());
			ver.addProperty("LONG", vert.getInfo().getLong());

			vertices.add("Vertice_"+ i, ver);
		}
		obj.addProperty("NUM_VERTICES", numVer);
		obj.add("Vertices", vertices);

		try {
			FileWriter file = new FileWriter("data/grafoRespaldo.json");
			file.write(obj.toString());
			file.flush();
			file.close();
		}
		catch(Exception e) 
		{
		}
	}
	/**
	 * Carga un grafo a partir de un formato json
	 */
	public void loadJson() 
	{
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(new FileReader("data/grafoRespaldo.json"));

			JSONObject jsonObject = (JSONObject) obj;

			grafoVias = new GrafoNoDirigido<String, CordVO, pesoWay>();

			long numVer = (long) jsonObject.get("NUM_VERTICES");
			long numArc = (long) jsonObject.get("NUM_ARCOS");

			JSONObject ver = (JSONObject) jsonObject.get("Vertices");			
			System.out.println("adg");
			JsonObject arc = (JsonObject) jsonObject.get("Arcos");
			System.out.println("aaa");
			JsonParser jsonParser=new JsonParser();
			for(int i = 0; i < numVer; i++) {
				JsonObject elem = (JsonObject) jsonParser.parse(ver.toString());;
				String id = elem.get("ID").getAsString();
				double lat = elem.get("LAT").getAsDouble();
				double lon = elem.get("LON").getAsDouble();
				CordVO cord = new CordVO(lat,lon);
				grafoVias.addVertex(id, cord);
			}
			for(int i = 0; i < numArc; i++) {
				JsonObject elem = (JsonObject) arc.get("Arco_"+i);
				String verID_I = elem.get("ID_I").getAsString();
				String verID_F = elem.get("ID_F").getAsString();
				double dist = elem.get("DISTANCIA").getAsDouble();
				String arcID = elem.get("ID_STREET").getAsString();
				pesoWay pw = new pesoWay(arcID,dist);
				grafoVias.addEdge(verID_I, verID_F, pw);
			}

		}catch (FileNotFoundException e) {
			//manejo de error
			System.out.println("f");
		} catch (IOException e) {
			//manejo de error
			System.out.println("i");
		} catch (ParseException e) {
			//manejo de error
			System.out.println("p");
		}
	}
	/**
	 * Crea y grafica el mapa 
	 * @param argumentos
	 */
	public void generarMapaGrafo(GrafoNoDirigido<String, CordVO, pesoWay> pGrafo,LatLng min,LatLng max)
	{
		Mapa x=new Mapa(grafoVias,min,max);
		Mapa.graficarMapa(x);
	}
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha);
	}
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
	/**
	 * Retorna los arcos de una cola pasada por parametro en un arreglo.
	 * @return
	 */
	private Arco<String, CordVO, pesoWay>[] getArregloArcos(Queue<Arco<String, CordVO, pesoWay>> cola)
	{
		IteratorQueue<Arco<String, CordVO, pesoWay>> it=cola.iterator();
		Arco<String, CordVO, pesoWay>[] infracciones=new Arco[cola.size()];
		int i=0;
		while(it.hasNext())
		{
			Arco<String, CordVO, pesoWay> x=it.next();
			infracciones[i]=x;
			i++;
		}
		return infracciones;
	}
}
