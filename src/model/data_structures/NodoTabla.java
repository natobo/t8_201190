package model.data_structures;

/**
 * Clase que representa un nodo dentro de la lista
 * @author nicot
 * @param <K> llave del nodo
 * @param <V> valor del nodo
 */
public class NodoTabla<K,V> 
{
	/**
	 * valor que el nodo contiene
	 */
	private V valor;
	
	/**
	 * llave del nodo 
	 */
	private K llave;
	
	/**
	 * Elemento siguiente al nodo. 
	 */
	private NodoTabla<K,V> siguiente;
	
	/**
	 * Constructor de la clase nodo
	 * @param dato elemento o dato a guardar en el nodo
	 * @param Nodo siguiente al que se esta creando.
	 */
	public NodoTabla(K pLlave, V pValor)
	{
		valor = pValor;
		llave = pLlave;
	}
	/**
	 * Constructor de la clase nodo
	 * @param dato valor o dato a guardar en el nodo
	 * @param Nodo siguiente al que se esta creando.
	 */
	public NodoTabla(K pLlave, V pValor,NodoTabla<K,V> pSiguiente)
	{
		valor = pValor;
		llave = pLlave;
		siguiente = pSiguiente;
	}
	/**
	 * Retorna el valor del nodo
	 * @return valor del nodo.
	 */
	public V getValor()
	{
		return valor;
	}
	/**
	 * Retorna la llave del nodo
	 * @return llave del nodo.
	 */
	public K getLlave()
	{
		return llave;
	}
	/**
	 * Cambia el valor del nodo.
	 * @param pvalor valor a asignar al nodo
	 */
	public void setValor(V pvalor)
    {
    	valor = pvalor;
    }
	/**
	 * Cambia la llave del nodo.
	 * @param pLlave llave a asignar al nodo
	 */
	public void setLlave(K pLlave)
    {
    	llave = pLlave;
    }
	/**
	 * Retorna el siguiente del nodo
	 * @return siguiente del nodo
	 */
	public NodoTabla<K,V> getSiguiente()
	{
		return siguiente;
	}
	/**
	 * Cambia el siguiente nodo de la lista.
	 * @param pNodo nodo a asignar como siguiente
	 */
	public void setSiguiente(NodoTabla<K,V> pNodo)
    {
    	siguiente = pNodo;
    }
}
