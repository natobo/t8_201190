package model.data_structures;

/**
 * Clase que representa una lista encadenada de NodosTabla
 * @author nicot
 * Referencias Codigo:: https://algs4.cs.princeton.edu/home/ (Pagina oficial del libro Algorithms, 4th Edition)
 * Adaptado de: https://algs4.cs.princeton.edu/31elementary/SequentialSearchST.java.html
 */
public class ListaNodoTabla <K,V>
{
     /**
      * Tama�o de la lista encadenada
      */
	private int numNodos;
	/**
	 * Primer nodo de la lista.
	 */
    private NodoTabla<K, V> primero;
    
    /**
     * Constructor de la lista
     */
    public ListaNodoTabla() 
    {
		// TODO Auto-generated constructor stub
	}
    /**
     * Retorna el tama�o de la lista
     * @return tama�o lista
     */
    public int darTamano() 
    {
        return numNodos;
    }
    
    /**
     * Retorna el valor de la llave pasada por parametro
     * @param  key Llave a buscar
     * @return Valor de la llave, null en caso de que no se encuentre.
     * @throws IllegalArgumentException si key pasada por parametro es nula
     */
    public V darValor(K key) 
    {
        if (key == null) throw new IllegalArgumentException("La llave pasada por parametro es nula"); 
        for (NodoTabla<K, V> x = primero; x != null; x = x.getSiguiente())
        {
            if (key.equals(x.getLlave()))
                return x.getValor();
        }
        return null;
    }
    
    /**
     *Inserta un nodoTabla dentro de la lista de nodos, si el nodo ya existia se sobrescribe el viejo valor del nodo 
     *con el nuevo pasado por parametro.<br>
     *Elimina el nodo de la lista si el valor pasado por Valor es nulo.
     * @param  key Llave del nodo
     * @param  val Valor del nodo 
     * @throws IllegalArgumentException si la llave es nula
     */
    public void agregarNodo(K key, V val) 
    {
        if (key == null) throw new IllegalArgumentException("la llave a colocar en la lista es nula"); 
        if (val == null) 
        {
            eliminar(key);
            return;
        }
//         ESTE CICLO SOBREESCRIBE LA INFORMACION
        for (NodoTabla<K, V>x = primero; x != null; x = x.getSiguiente()) 
        {
            if (key.equals(x.getLlave()))
            {
                x.setValor(val);
                return;
            }
        }
        primero = new NodoTabla<K, V>(key, val,primero);
        numNodos++;
    }
    
    /**
     * Remueve el nodo con la llave pasada por parametro.
     * @param  key Llave del nodo a eliminar
     * @throws IllegalArgumentException si la llave es nula
     */
    public void eliminar(K key) 
    {
        if (key == null) throw new IllegalArgumentException("la llave a eliminar es nula"); 
        primero = eliminarNodo(primero, key);
    }

   /**
    * Busca en la lista de nodos la llave a eliminar , si encuentra el nodo con la llave a eliminar, cambia la referencia del anterior eliminandolo en el proceso<br>
    * Este metodo se realiza por medio de recursion
    * @param nod Primer nod anterior
    * @param key LLave del nodo a eliminar
    * @return Lista de nodos encadenada con el nodo eliminado  
    */
    private NodoTabla<K, V>eliminarNodo(NodoTabla<K, V>nod, K key) 
    {
        if (nod == null) return null;
        if (key.equals(nod.getLlave())) 
        {
            numNodos--;
            return nod.getSiguiente();
        }
        nod.setSiguiente(eliminarNodo(nod.getSiguiente(), key));
        return nod;
    }
    
    /**
     * Retorna una cola con todas las llaves de la lista
     * To iterate over all of the keys in the symbol table named {@code st},
     * use the foreach notation: {@code for (Key key : st.keys())}.
     * @return all keys in the symbol table
     */
    public Iterable<K> keys()  
    {
        Queue<K> queue = new Queue<K>();
        for (NodoTabla<K, V> x =primero ; x != null; x = x.getSiguiente())
            queue.enqueue(x.getLlave());
        return queue;
    }
    /**
     * Retorna verdadero si la lista de nodos contiene la llave pasada pr parametro.
     * @param  key Llave a buscar
     * @return True si la tabla contiene la llave 
     *         False de lo contrario.
     * @throws IllegalArgumentException si la llave a buscar llega ser nula
     */
    public boolean contiene(K key) 
    {
        if (key == null) throw new IllegalArgumentException("La llave a buscar es nula");
        return darValor(key)!= null;
    }
}
