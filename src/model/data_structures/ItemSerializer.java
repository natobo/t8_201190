package model.data_structures;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import model.data_structures.GrafoNoDirigido;
/**
 * Serializador de formato Json 
 * @author nicot
 */
public class ItemSerializer extends StdSerializer<GrafoNoDirigido> 
{

	public ItemSerializer() 
	{
		this(null);
	}

	public ItemSerializer(Class<GrafoNoDirigido> t) 
	{
		super(t);
	}

	@Override
	public void serialize(
			GrafoNoDirigido value, JsonGenerator jgen, SerializerProvider provider) 
					throws IOException, JsonProcessingException {

		jgen.writeStartObject();
		jgen.writeNumberField("numVertices", value.V());
		jgen.writeNumberField("numArcos", value.E());
	    
	}
}
