package model.data_structures;

/**
 * 
 * Clase que representa un grafo no dirigido 
 * @author nicot
 *
 * @param <K> Llaves de los vertices
 * @param <V> Valor a guardar por los vertices
 * @param <A> Peso, o informacion de los Arcos
 */
public class GrafoNoDirigido<K extends Comparable<K>,V,A> implements IGrafoNoDirigido <K,V,A> 
{
	/**
	 * Cantidad de vertices 
	 */
	private int numVertices;
	/**
	 * Cantidad de Arcos
	 */
	private int numArcos;	
	/**
	 * Tabla de hash que contiene los vertices
	 */
	private HashSeparateChaining<K, Vertice<K, V,A>> verticesGrafo;
	/**
	 * Arreglo que representa todas las llaves de los vertices del grafo
	 */
	private ArregloDinamico<K> keys;
	/**
	 * Cola que guarda todos los arcos del grafo
	 */
	private Queue<Arco<K,V,A>> arcos; 
	/**
	 *Constructor de grafo no dirigido. 
	 */
	public GrafoNoDirigido() 
	{
		// TODO Auto-generated constructor stub
		verticesGrafo=new HashSeparateChaining<K, Vertice<K,V,A>>(100000);
		numArcos=0;
		numVertices=0;
		keys = new ArregloDinamico<K>(500);
		arcos = new Queue<Arco<K,V,A>>();
	}
	/**
	 * Retorna la cola con los arcos 
	 * @return queue arcos
	 */
	public Queue<Arco<K,V,A>> darArcos()
	{
		return arcos;
	}
	/**
	 * Retorna el arreglo de Keys 
	 * @return ArregloDinamico de llaves del grafo.
	 */
	public ArregloDinamico<K> keys() 
	{
		return keys;
	}
    /**
     * Retorna el vertice con el id pasado por parametro
     * @param idVertex. Id del vertice a buscar en el grafo
     * @return vertice dentro del grafo
     */
	public Vertice<K,V,A> darVertice(K idVertex) 
	{
		return verticesGrafo.get(idVertex);
	}

	@Override
	public int V() 
	{
		// TODO Auto-generated method stub
		return numVertices;
	}

	@Override
	public int E() 
	{
		// TODO Auto-generated method stub
		return numArcos;
	}
	@Override
	public void addVertex(K idVertex, V infoVertex) 
	{
		// TODO Auto-generated method stub
		Vertice<K,V,A> x=new Vertice<K,V,A>(idVertex, infoVertex);
		verticesGrafo.put(idVertex, x);
		numVertices++;
		keys.agregar(idVertex);
	}
	@Override
	public V getInfoVertex(K idVertex) 
	{
		// TODO Auto-generated method stub
		return verticesGrafo.get(idVertex).getInfo();
	}
	@Override
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) 
	{
		// TODO Auto-generated method stub
		Vertice<K, V,A> pInicial=verticesGrafo.get(idVertexIni);
		Vertice<K, V,A> pFinal=verticesGrafo.get(idVertexFin);
		Arco<K, V, A> arco=new Arco<K,V,A>(pInicial, pFinal, infoArc);		
		pInicial.agregarArco(arco); //Error aqui estoy a�adiendo dos veces en la misma cola un arco osea A a B y B a A
		pFinal.agregarArco(arco);
		numArcos++;
		arcos.enqueue(arco);;
	}
	@Override
	public void setInfoVertex(K idVertex, V infoVertex) 
	{
		// TODO Auto-generated method stub
		verticesGrafo.get(idVertex).setInfo(infoVertex);
	}
	@Override
	public A getInfoArc(K idVertexIni, K idVertexFin) 
	{
		// TODO Auto-generated method stub
		A rta=null;
		Queue<Arco<K, V, A>> adjInicial=verticesGrafo.get(idVertexIni).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it=adjInicial.iterator();
		while (it.hasNext()) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			if(arco.getFinal().equals(verticesGrafo.get(idVertexFin)))
			{
				rta=arco.getInfoArco();
				break;
			}
		}
		return rta;
	}

	@Override
	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) 
	{
		// TODO Auto-generated method stub
		Queue<Arco<K, V, A>> adjInicial=verticesGrafo.get(idVertexIni).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it=adjInicial.iterator();
		while (it.hasNext()) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			if(arco.getFinal().equals(verticesGrafo.get(idVertexFin))) //Duda si toca preguntar el inicial. arco.getInicial().equals(verticesGrafo.get(idVertexIni))&&
			{
				arco.setInfoArco(infoArc);
				break;
			}
		}
	}

	@Override
	public IteratorQueue<K> adj(K idVertex) 
	{
		// TODO Auto-generated method stub
		Queue<K> colaRta=new Queue<K>();
		Queue<Arco<K, V, A>> adjInicial=verticesGrafo.get(idVertex).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it=adjInicial.iterator();
		while (it.hasNext()) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			colaRta.enqueue(arco.getFinal().getId());
		}
		return colaRta.iterator();
	}

	/**
	 * Retorna el numero de vertices y el numero de Arcos del graf
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();

		s.append(numVertices + " vertices, " + numArcos + " arcos ");

		return s.toString();
	}
}
