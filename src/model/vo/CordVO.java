package model.vo;

//vo ES VALUE OBJECT
public class CordVO implements Comparable<CordVO>
{
	/**
	 * Coordernada en x
	 */
    public Double Lat;
	/**
	 * Coordernada en y
	 */
    public Double Long;
  
	/**
	 * Constructor de la clase
	 */
	public CordVO(Double pLat,Double pLong ) 
	{
		Lat=pLat;
		Long=pLong;
	}

	/**
	 * Retorna la coordenada en X	
	 * @return
	 */
    public Double getLat() 
    {
		return Lat;
	}
	/**
	 * Retorna la coordenada en Y	
	 * @return
	 */
	public Double getLong() 
	{
		return Long;
	}
	
	/**
	 * Compara dos LovationVo por su numero de registros si son iguales los compara por orden alfabetico de sus locaciones.
	 */
	@Override
	public int compareTo(CordVO o) 
	{
		// TODO Auto-generated method stub
		//Rta es positivo si el numero de registros es mayor al que me pasan por parametro.
		//Rta es negativo si el numeor de registros es menor al que me pasan por parametro.
		int rta=Lat.compareTo(o.Lat);
		if(rta==0)
		{
		   rta=Long.compareTo(o.Long);	
		}
		return rta;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return Lat+","+Long;
	}
}
