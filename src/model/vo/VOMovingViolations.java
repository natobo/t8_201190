package model.vo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{

	/**
	 * objectId del auto
	 */
	private String objectId;
	/**
	 * violationDescription del auto
	 */
	private String violationDescription;
	/**
	 * totalPaid del auto
	 */
	private String totalPaid;
	/**
	 * accidentIndicator del auto.
	 */
	private String accidentIndicator;
	/**
	 * ticketIssueDate del auto.
	 */
	private LocalDateTime ticketIssueDate;
	/**
	 * violationCode del auto.
	 */
	private String violationCode;
	/**
	 * FineAMT de la infraccion.
	 */
	private String FineAMT;
	/**
	 * Direccion donde ocurrio la infraccion.
	 */
	private Integer Streetsegid ; 
	/**
	 * Address_Id de la infraccion.
	 */
	private Integer Address_Id;  
	/**
	 * Multa 1 de la infraccion
	 */
	private String penalty1;
	/**
	 * Multa 2 de la infraccion
	 */
	private String penalty2;
	/**
	 * Locacion de la infraccion
	 */
	private String location;
	/**
	 * Coordenanda en X de la infraccion.
	 */
	private String Xcord;
	/**
	 * Coordenanda en Y de la infraccion.
	 */
	private String Ycord;

	/**
	 * constructor
	 */
	public VOMovingViolations(String pObjectId,String pViolationDescription,String pLocation,String pTotalPaid,String pAccidentIndicator,LocalDateTime pTicketIssueDate,String pViolationCode,String pFineAMT,Integer pStreet, Integer pAddress_Id,String pPenalty1,String pPenalty2,String pXcord,String pYcord)
	{ 
		objectId=pObjectId;
		violationDescription=pViolationDescription;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		ticketIssueDate=pTicketIssueDate;
		violationCode=pViolationCode;
		FineAMT=pFineAMT;
		Streetsegid=pStreet;
		Address_Id=pAddress_Id;
		penalty1=pPenalty1;
		penalty2=pPenalty2;
		location=pLocation;
		Xcord=pXcord;
		Ycord=pYcord;
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int getObjectId() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(objectId);
	}	

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() 
	{
		// TODO Auto-generated method stub
		return location ;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public LocalDateTime getTicketIssueDate() 
	{
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() 
	{
		// TODO Auto-generated method stub
		return Integer.parseInt(totalPaid);
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() 
	{
		// TODO Auto-generated method stub
		return violationDescription;
	}
	/**
	 * Retorna el StreetId de la infraccion
	 * @returnStreetId de la infraccion
	 */
	public Integer getStreetSegId()
	{
		return Streetsegid;
	}
	/**
	 * Retorna el codigo de violacion de la infraccion
	 * @return codigo de violacion de la infraccion
	 */
	public String getViolationCode()
	{
		return violationCode;
	}

	/**
	 * Retorna el FineAMT de la infraccion
	 * @return FineAMT de la infraccion
	 */
	public String getFineAMT()
	{
		return FineAMT;
	}

	/**
	 * Retorna el Address_Id de la infraccion
	 * @return Address_Id de la infraccion
	 */
	public Integer getAddressId() 
	{
		return Address_Id;
	}
	/**
	 * Retorna el penalty1 de la infraccion
	 * @return penalty1 de la infraccion
	 */
	public String getPenalty1()
	{
		return penalty1;
	}
	/**
	 * Retorna el penalty2 de la infraccion
	 * @return penalty2 de la infraccion
	 */
	public String getPenalty2()
	{
		return penalty2;
	}
	/**
	 * Retorna la Xcord de la infraccion
	 * @return Xcord de la infraccion
	 */
	public Double getXcord()
	{
		return  Double.parseDouble(Xcord);
	}
	/**
	 * Retorna la Ycord de la infraccion
	 * @return Ycord de la infraccion
	 */
	public Double getYcord()
	{
		return Double.parseDouble(Ycord);
	}

	@Override
	public int compareTo(VOMovingViolations o) 
	{
		// TODO Auto-generated method stub
		int rta=0;

		if(o!=null)
		rta= ticketIssueDate.compareTo(o.getTicketIssueDate());

		return rta;
	}

	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "La infraccion: "+objectId + " tienen como adressId: "+Address_Id+" ,su location es: "+location +" ,su Streetsegid es: "+Streetsegid+", su coordenada en x es: "+Xcord+" ,su coordenada en y es: "+Ycord+"  y su ticketIssueDate es: "+ticketIssueDate;
	}
}
