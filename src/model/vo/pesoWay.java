package model.vo;
/**
 * Clase que representa la informacion que alberga cada arco
 * @author nicot
 */
public class pesoWay 
{
	/**
	 * Id del way
	 */
	private String id;
	/**
	 * Distancia entre los dos vertices
	 */
	private Double distHarvesiana;

	/**
	 * Constructor de la clase
	 */
	public pesoWay(String pId,Double pDist) 
	{
		id=pId;
		distHarvesiana=pDist;
	}

	/**
	 * Obtiene el id del way
	 * @return
	 */
	public String getId() 
	{
		return id;
	}
    /**
     * define el id del arco
     * @param id
     */
	public void setId(String id) 
	{
		this.id = id;
	}
    /**
     * Obtiene la distancia harvesiana del arco
     * @return
     */
	public Double getDistHarvesiana() 
	{
		return distHarvesiana;
	}
    /**
     * Define la distancia cartesiana.
     * @param distHarvesiana
     */
	public void setDistHarvesiana(Double distHarvesiana) 
	{
		this.distHarvesiana = distHarvesiana;
	}
	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return "El id del way al que pertence este arco es: "+id+" ,la distancia harvesiana de este arco es: "+distHarvesiana;
	}
	
	
}
