package main;

import controller.Controller;

public class MVC 
{
	/**
	 * Clase que controla la aplicacion
	 */
	static Controller controler ;
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		controler = new Controller();
		controler.run();
	}
}
